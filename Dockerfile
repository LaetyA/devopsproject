
FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
# Installer les dépendances
RUN npm install
# Copier le reste des fichiers 
COPY . .
# Exposer le port 
EXPOSE 3000
# Commande pour démarrer l'application
CMD ["node", "app.js"]
